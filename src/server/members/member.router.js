import { Router } from "express";
import { memberController } from "./member.controller.js";

const router = Router()

router.get('/members', memberController.getAll)

export default router